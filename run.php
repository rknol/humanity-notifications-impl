<?php
require_once('./vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Humanity\Notifications\NotificationManager;
use Humanity\NotificationsImpl\User;
use Humanity\NotificationsImpl\Notification\NewComment;

// Create an Actor to dispatch notification to
$user = new User('Ruben', 'Knol', 'ruben@turnware.rs', '+381637549661');

// Create a logger that logs to stdout
$logger = new Logger('notification-sender');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

// Dispatch a notification (phone + SMS)
$notificationManager = new NotificationManager($logger);
$notification = new NewComment('My Blog post', 'Bob A.');
$notificationManager->send($user, $notification);
