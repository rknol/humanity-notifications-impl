<?php
namespace Humanity\NotificationsImpl;

use Humanity\Notifications\ActorInterface;

/**
 * Class User
 *
 * @package Humanity\NotificationsImpl
 * @author Ruben Knol <ruben@turnware.rs>
 */
class User implements ActorInterface
{
	/**
	 * @var string
	 */
	protected $firstName;

	/**
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var string
	 */
	protected $emailAddress;

	/**
	 * @var string
	 */
	protected $phoneNumber;

	/**
	 * Constructor
	 *
	 * @param string $firstName
	 * @param string $lastName
	 * @param string $emailAddress
	 * @param string $phoneNumber
	 */
	public function __construct($firstName, $lastName, $emailAddress, $phoneNumber)
	{
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->emailAddress = $emailAddress;
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * Get the name of the User
	 * @return string
	 */
	public function getName()
	{
		return sprintf('%s %s', $this->firstName, $this->lastName);
	}

	/**
	 * Get the e-mail address of the User
	 * @return string
	 */
	public function getEmailAddress()
	{
		return $this->emailAddress;
	}

	/**
	 * Get the phone number of the User
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}
}