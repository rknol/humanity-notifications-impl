<?php
namespace Humanity\NotificationsImpl\Notification;

use Humanity\Notifications\NotificationInterface;

/**
 * Class NewComment
 *
 * @package Humanity\NotificationsImpl\Notification
 * @author Ruben Knol <ruben@turnware.rs>
 */
class NewComment implements NotificationInterface
{
	/**
	 * @var string The post name
	 */
	protected $post;

	/**
	 * @var string The author's name
	 */
	protected $author;

	/**
	 * Construct a new notification
	 * @param string $post
	 * @param string $author
	 */
	public function __construct($post, $author)
	{
		$this->post = $post;
		$this->author = $author;
	}

	/**
	 * Retrieve the post name
	 * @return string
	 */
	public function getPost()
	{
		return $this->post;
	}

	/**
	 * Retrieve the author's name
	 * @return string
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * Register the notification senders for this notification
	 * @return array
	 */
	public function registerNotificationSenders()
	{
		return [
			new NewComment\Email(),
			new NewComment\Sms(),
		];
	}
}