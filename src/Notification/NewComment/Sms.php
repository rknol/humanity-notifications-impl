<?php
namespace Humanity\NotificationsImpl\Notification\NewComment;

use Humanity\Notifications\ActorInterface;
use Humanity\Notifications\NotificationInterface;
use Humanity\Notifications\NotificationSender;
use Humanity\NotificationsImpl\Notification\NewComment;
use Monolog\Logger;

class Sms extends NotificationSender
{
	/**
	 * Set the notification sender adapter for this notification sender
	 * @return \Humanity\Notifications\NotificationSenderAdapter\Sms
	 */
	public function setNotificationSenderAdapter()
	{
		return new \Humanity\Notifications\NotificationSenderAdapter\Sms();
	}

	/**
	 * Send the notification through our notification sender adapter
	 * @param ActorInterface $actor
	 * @param NewComment $notification
	 * @param Logger $logger
	 */
	public function send(ActorInterface $actor, NotificationInterface $notification, Logger $logger)
	{
		/**
		 * @var \Humanity\Notifications\NotificationSenderAdapter\Sms $adapter
		 */
		$adapter = $this->getNotificationSenderAdapter();
		$adapter->setPhoneNumber($actor->getPhoneNumber());
		$adapter->setMessage(sprintf("New comment on post '%s' by '%s'",
			$notification->getPost(), $notification->getAuthor()));

		$adapter->send($logger);
	}
}