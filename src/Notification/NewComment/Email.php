<?php
namespace Humanity\NotificationsImpl\Notification\NewComment;

use Humanity\Notifications\ActorInterface;
use Humanity\Notifications\NotificationInterface;
use Humanity\Notifications\NotificationSender;
use Humanity\NotificationsImpl\Notification\NewComment;
use Monolog\Logger;

/**
 * Class Email
 *
 * @package Humanity\NotificationsImpl\Notification\NewComment
 * @author Ruben Knol <ruben@turnware.rs>
 */
class Email extends NotificationSender
{
	/**
	 * Set the notification sender adapter for this notification sender
	 * @return \Humanity\Notifications\NotificationSenderAdapter\Email
	 */
	public function setNotificationSenderAdapter()
	{
		return new \Humanity\Notifications\NotificationSenderAdapter\Email();
	}

	/**
	 * Send the notification through our notification sender adapter
	 * @param ActorInterface $actor
	 * @param NewComment $notification
	 * @param Logger $logger
	 */
	public function send(ActorInterface $actor, NotificationInterface $notification, Logger $logger)
	{
		/**
		 * @var \Humanity\Notifications\NotificationSenderAdapter\Email $adapter
		 */
		$adapter = $this->getNotificationSenderAdapter();
		$adapter->setTo($actor->getEmailAddress());
		$adapter->setSubject(sprintf("New comment on post '%s'", $notification->getPost()));
		$adapter->setBody(sprintf("The user '%s' has left a comment on your blog post '%s'",
			$notification->getAuthor(), $notification->getPost()));

		$adapter->send($logger);
	}
}