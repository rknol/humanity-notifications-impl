# Notification sender example application

For the assignment I've constructed this example application that demonstrates the following:

* Uses composer to download & install the library that powers the example application
* Implements the 'adapter' design pattern
* Full object oriented design with mostly traits common to static programming
* Using a Monolog logger object that is used to log useful information

It's a commandline application, so please refer to the 'usage' heading on how to run

## Installation

To install the example run the following command:

```
$ composer install
```

## Usage

To run the example application, execute the following command:

```
$ php run.php
```

You should now see Monolog logger output to the screen that illustrates the application works